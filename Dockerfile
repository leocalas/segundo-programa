FROM python:3.8.0b4-alpine3.10
#RUN apk add --no-cache mysql
#RUN apk add --no-cache mysql-client
WORKDIR /usr/src/app
COPY ./src .
 
RUN pip install --no-cache-dir -r requirements.txt
CMD [ "python", "./main.py" ]